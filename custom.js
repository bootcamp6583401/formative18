let slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides((slideIndex += n));
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
}

const dreamerTop = document.querySelector(".dreamer__top");
const dreamerBot = document.querySelector(".dreamer__bottom");

const topColor = "rgb(85, 162, 166)";
const botColor = "#515050";

console.log(dreamerBot, dreamerBot);

console.log("Sewagg");

if (dreamerTop && dreamerBot) {
  dreamerTop.addEventListener("mouseover", () => {
    dreamerTop.style.backgroundColor = botColor;
    dreamerBot.style.backgroundColor = topColor;
  });

  dreamerBot.addEventListener("mouseover", () => {
    dreamerBot.style.backgroundColor = topColor;
    dreamerTop.style.backgroundColor = botColor;
  });

  dreamerTop.addEventListener("mouseout", () => {
    dreamerTop.style.backgroundColor = topColor;
    dreamerBot.style.backgroundColor = botColor;
  });

  dreamerBot.addEventListener("mouseout", () => {
    dreamerBot.style.backgroundColor = botColor;
    dreamerTop.style.backgroundColor = topColor;
  });
}
